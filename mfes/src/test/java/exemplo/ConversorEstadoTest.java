package exemplo;

import static org.junit.Assert.assertEquals;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.function.Executable;

public class ConversorEstadoTest {

	private static ConversorEstado subject;

	@BeforeClass
	public static void beforeClass() {
	}

	@Before
	public void before() {
		subject = new ConversorEstado();
	}

	@Test
	public void testConverterACParaAcre() {
		String nomeEstado = subject.converter("AC");

		assertEquals("Acre", nomeEstado); // os dois asserts fazem a mesma coisa
		Assert.assertTrue("Acre".equals(nomeEstado)); //
	}

	@Test
	public void testConverterALParaAlagoas() {
		ConversorEstado conversorEstado = new ConversorEstado();

		String nomeEstado = conversorEstado.converter("AL");

		assertEquals("Alagoas", nomeEstado);
	}

	@Test
	public void testUfIsNull() {
		Assertions.assertThrows(UFInexistenteException.class, new Executable() {

			public void execute() throws Throwable {
				subject.converter(null);
			}
		});
	}

	@Test
	public void testUfIsEmpty() {
		Assertions.assertThrows(UFInexistenteException.class, new Executable() {

			public void execute() throws Throwable {
				subject.converter("");
			}
		});	
	}

	@Test
	public void testUfIsInexistenteWW() {
		Assertions.assertThrows(UFInexistenteException.class, new Executable() {

			public void execute() throws Throwable {
				subject.converter("WW");
			}
		});
	}

	@Test
	public void testUfIsInexistenteAA() {
		Assertions.assertThrows(UFInexistenteException.class, new Executable() {

			public void execute() throws Throwable {
				subject.converter("AA");
			}
		});
	}

}
