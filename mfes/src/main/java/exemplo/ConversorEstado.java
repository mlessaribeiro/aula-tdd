package exemplo;

import java.util.HashMap;
import java.util.Map;

public class ConversorEstado {

	Map<String, String> estados = new HashMap<String, String>();
	
	public ConversorEstado() {
		estados.put("AC", "Acre");
		estados.put("AL", "Alagoas");
		estados.put("AP", "Amapá");
		estados.put("AM", "Amazonas");
	}
	
	public String converter(String uf) {
		String nomeEstado = estados.get(uf);
		if (nomeEstado != null)
			return nomeEstado;
		
		throw new UFInexistenteException();
	}
	
}
